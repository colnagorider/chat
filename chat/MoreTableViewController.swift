//
//  MoreTableViewController.swift
//  Chat
//
//  Created by Rajkumar Sharma on 04/09/14.
//  Copyright (c) 2014 MyAppTemplates. All rights reserved.
//

import UIKit

class MoreTableViewController: UITableViewController {

    var user: ReGaUser?
    var field: NSDictionary?
    var items:[NSDictionary]?
    var selected: NSIndexPath?
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

    @IBOutlet var tblOptions : UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        self.tblOptions?.tableFooterView = UIView(frame: CGRectZero)
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.selected = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        
        if(items != nil) {
            return items!.count
        }
        return 1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // Configure the cell...
        let cell = tblOptions!.dequeueReusableCellWithIdentifier("SelectCell") as UITableViewCell!
        if let label = cell.subviews[0].subviews.filter({$0.tag == 0})[0] as? UILabel {
            if(items != nil) {
                label.text = items![indexPath.row]["name"] as! String?
            }
            if let selected = self.selected {
                if(selected.row == indexPath.row) {
                    cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                }
                else {
                    cell.accessoryType = UITableViewCellAccessoryType.None
                }
            }
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        tblOptions!.cellForRowAtIndexPath(indexPath)!.accessoryType = UITableViewCellAccessoryType.Checkmark
        if(self.selected != nil) {
            tblOptions!.cellForRowAtIndexPath(self.selected!)!.accessoryType = UITableViewCellAccessoryType.None
        }
        self.selected = indexPath
    }

    override func viewWillDisappear(animated: Bool) {
        if let selected = self.selected {
            if let user = self.user {
                if let items = self.items {
                    let item = items[selected.row]
                    if let field = self.field {
                        print("Selected item is \(selected.row) value \(item["name"])" )
                        user.setFieldValue(field["id"] as! Int, withInt: selected.row)
                    }
                }
            }
            
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        if let field = self.field {
            if let user = self.user {
                if let value = user.getFieldValue(field["id"] as! Int) {
                    if let items = field["items"] as! [NSDictionary]? {
                        if let item = items.filter({ $0["value"] as! Int == value["value"] as! Int }) as [NSDictionary]? {
                            if(item.count > 0) {
                                if let index = items.indexOf(item[0]) {
                                    print("Selected index is \(index)" )
                                    self.selected = NSIndexPath(forRow: index, inSection: 0)
                                }
                            }
                        }
                    }
                }
                if let title = field["name"] as! String? {
                    navigationItem.title = title
                    self.title = title
                }
            }
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView!, moveRowAtIndexPath fromIndexPath: NSIndexPath!, toIndexPath: NSIndexPath!) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView!, canMoveRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
