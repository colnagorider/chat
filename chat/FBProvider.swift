//
//  FBProvider.swift
//  chat
//
//  Created by Sergei Sevriugin on 11/09/16.
//  Copyright © 2016 MyAppTemplates. All rights reserved.
//

import Foundation
class FBProvider: Provider {
    let redirectUri:String = "http://rega.site"
    override init(sync: Synchronizer) {
        super.init(sync: sync, withName:"Facebook", apiPath: "/auth/facebook")
    }
    override init(sync : Synchronizer, withDelegate delegate: ProviderDelegate) {
        super.init(sync: sync, withName:"Facebook", apiPath: "/auth/facebook", withDelegate: delegate)
    }
    override func sync(data: [NSDictionary]) {
        let facebook_data               = NSMutableDictionary()
        facebook_data["clientId"]       = data[0]["clientId"]
        facebook_data["accessToken"]    = data[0]["accessToken"]
        facebook_data["redirectUri"]    = self.redirectUri
        super.sync([facebook_data])
    }
}