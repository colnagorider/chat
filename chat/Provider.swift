//
//  Provider.swift
//  chat
//
//  Created by Sergei Sevriugin on 07/09/16.
//  Copyright © 2016 MyAppTemplates. All rights reserved.
//

import Foundation

enum ProviderStatus {
    case NotSynced
    case Error
    case Success
}

protocol ProviderDelegate {
    func providerDataIsReady(name: String, data:[NSDictionary]?, status: ProviderStatus)
}

class Provider : NSObject {
    
    var sync: Synchronizer
    var name: String = "Authenticate"
    var api : String = "/auth/login"
    var tag : String = "fields"
    
    var error: NSError?
    var delegate : ProviderDelegate?
    var data: [NSDictionary]?
    
    
    init(sync : Synchronizer) {
        self.sync = sync
        
        super.init()
        self.sync.addProvider(self)
    }
    
    init(sync : Synchronizer, withDelegate delegate: ProviderDelegate) {
        self.sync = sync
        self.delegate = delegate
        
        super.init()
        self.sync.addProvider(self)
    }
    
    init(sync : Synchronizer, withName name: String, apiPath api: String, withDelegate delegate: ProviderDelegate) {
        
        self.name = name
        self.sync = sync
        self.api = api
        self.delegate = delegate
        
        super.init()
        self.sync.addProvider(self)
    }
    
    init(sync : Synchronizer, withName name: String, apiPath api: String) {
        
        self.name = name
        self.sync = sync
        self.api = api
        
        super.init()
        self.sync.addProvider(self)
    }
    
    init(sync : Synchronizer, withName name: String, apiPath api: String, resultTag tag: String) {
       
        self.name = name
        self.sync = sync
        self.api = api
        self.tag = tag
        
        super.init()
        self.sync.addProvider(self)
    }
    
    func sync(data:[NSDictionary]) {
        self.data = data
        sync.resolve(self.name)
    }
    
    func resolve() {
        sync.resolve(self.name)
    }
    
    func solve(data: NSData?) -> [NSDictionary] {
        if let jsonData = data {
            do {
                if let jsonResult: NSDictionary = try NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary {
                    
                    if let responseResult : NSDictionary = jsonResult["result"] as? NSDictionary {
                        
                        if let result: [NSDictionary] = responseResult[self.tag] as? [NSDictionary] {
                            print("Provider \(self.name) resolve \(self.tag) : \(result)")
                            return result;
                        }
                        else {
                            print("Provider \(self.name) resolve result : \(responseResult)")
                            // set token
                            if let token = responseResult["token"] as! String? {
                                self.sync.mToken = token
                                print("Provider \(self.name) resolve set token for Synchronizer")
                            }
                            // set userInfo
                            let userInfo = UserInfo(
                                                    name: responseResult["name"] as! String?,
                                                    email: nil,
                                                    userId: responseResult["userId"] as! Int?,
                                                    avatar: responseResult["picture"] as! String?,
                                                    shareCode: responseResult["shareCode"] as! Int?
                                                    )
                            self.sync.user = userInfo
                            print("Provider \(self.name) set UserInfo :\(self.sync.user)")
                            return [responseResult];
                        }
                    }
                    else {
                        if let result : [NSDictionary] = jsonResult["result"] as? [NSDictionary] {
                            print("Provider \(self.name) resolve result : \(result)")
                            return result;
                        }
                    }
                }
            }
            catch { print("Provider \(self.name) error while parsing data result: \(error)") }
        }
        return [NSDictionary]()
    }
    
    func resolveCallback(data: NSData?, didReceiveResponse response: NSURLResponse?, error: NSError?) {
        if let httpResponse = response as? NSHTTPURLResponse {
            if httpResponse.statusCode != 200 {
                print("Provider \(self.name) resolveCallback response was not 200: \(response)")
                print("Provider \(self.name) resolveCallback data: \(data)")
                
                if let delegate = self.delegate {
                    delegate.providerDataIsReady(self.name, data: solve(data), status: ProviderStatus.Success)
                }
                return
            }
            else {
                print("Provider \(self.name) resolveCallback response was 200: \(response)")
                print("Provider \(self.name) resolveCallback data: \(data)")
                
                if let delegate = self.delegate {
                    delegate.providerDataIsReady(self.name, data: solve(data), status: ProviderStatus.Success)
                }
                return
            }
        }
        
        if(error != 0) {
            print("Provider \(self.name) resolveCallback error: \(error)")
            
            if let delegate = self.delegate {
                self.error = error
                delegate.providerDataIsReady(self.name, data: nil as [NSDictionary]?, status: ProviderStatus.Error)
            }
        }
    }
    
    func getApiPath() -> String {
        return self.api
    }
    
    func body() -> NSData {
        var body : NSData
        
        if let data = self.data {
            do {
                if(data.count == 1) {
                    body = try NSJSONSerialization.dataWithJSONObject(data[0], options: NSJSONWritingOptions(rawValue: 0))
                }
                else {
                    body = try NSJSONSerialization.dataWithJSONObject(data, options: NSJSONWritingOptions(rawValue: 0))
                }
            }
            catch {
                print("Provider \(self.name) body func Object to JSON error")
                return NSData()
            }
        }
        else {
            return NSData()
        }
        return body
    }
    
    func prepare(request: NSMutableURLRequest) {
        let body = self.body()
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("gzip", forHTTPHeaderField: "Accept-encoding")
        request.setValue(String(format: "%d",body.length), forHTTPHeaderField: "Content-Length")
        request.HTTPBody = body
    }
    
}
