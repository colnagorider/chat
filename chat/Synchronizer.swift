//
//  Synchronizer.swift
//  chat
//
//  Created by Sergei Sevriugin on 07/09/16.
//  Copyright © 2016 MyAppTemplates. All rights reserved.
//

import Foundation

enum SynchronizerStatus {
    case OK
    case REFUSED
    case ERROR
    case UNAUTHORIZED
    case NOT_REQUIRED
    case OFFLINE
}

struct UserInfo {
    let name: String?
    let email: String?
    let userId: Int?
    let avatar: String?
    let shareCode : Int?
}

class Synchronizer : NSObject, NSURLSessionDelegate {
    
    let URL_PREFIX = "https://rega.site/api-client"
    
    let CONNECTION_TIMEOUT = 20.0
    let READ_TIMEOUT = 15000
    
    var mToken: String? = nil
    var mProviders = [String:Provider]()
    
    var session:NSURLSession?
    var user: UserInfo?
    
    static func getSharedInstance() -> Synchronizer? {
        let app = UIApplication.sharedApplication().delegate as! AppDelegate
        return app.synchronizer
    }
    
    func getProvider(name:String) -> Provider? {
        return mProviders[name]
    }
    
    func prepare(request:NSMutableURLRequest) {
        request.timeoutInterval = CONNECTION_TIMEOUT
        request.HTTPMethod = "POST"
        request.HTTPShouldHandleCookies = false
        
        if let token = self.mToken {
            request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        }
    }
    
    func resolve(name:String) -> SynchronizerStatus {
        if let provider = self.getProvider(name) {
            
            // prepare request path
            let path = URL_PREFIX + provider.getApiPath()
            // create requets
            let request = NSMutableURLRequest(URL:NSURL(string: path)!)
            // prepare request
            self.prepare(request)
            provider.prepare(request)
            
            print("Synchronizer resiolve request \(request)")
            
            if let session = self.session {
                let task = session.dataTaskWithRequest(request, completionHandler: provider.resolveCallback)
                task.resume()
                return SynchronizerStatus.OK
            }
        }
        return SynchronizerStatus.ERROR
    }
    
    func open() {
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        self.session = NSURLSession(configuration: configuration, delegate: self, delegateQueue:NSOperationQueue.mainQueue())
        print("Synchronizer session is open")
    }
    
    func addProvider(provider: Provider) {
        mProviders[provider.name] = provider
        print("Synchronizer provider \(provider.name) installed")
    }
    
    func URLSession(session: NSURLSession, didReceiveChallenge challenge: NSURLAuthenticationChallenge, completionHandler: (NSURLSessionAuthChallengeDisposition, NSURLCredential?) -> Void) {
        
        if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
            
            let serverTrust:SecTrustRef = challenge.protectionSpace.serverTrust!
            let credential:NSURLCredential = NSURLCredential(forTrust: serverTrust)
            
            challenge.sender?.useCredential(credential, forAuthenticationChallenge: challenge)
                
            completionHandler(NSURLSessionAuthChallengeDisposition.UseCredential, NSURLCredential(forTrust: challenge.protectionSpace.serverTrust!))
            
            print("Synchronizer didReceiveChallenge server authorized")
        }
        else {
            completionHandler(NSURLSessionAuthChallengeDisposition.CancelAuthenticationChallenge, nil);
            print("Synchronizer didReceiveChallenge \(challenge.protectionSpace.authenticationMethod) canceled ")
        }
    }
}