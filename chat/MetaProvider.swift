//
//  MetaProvider.swift
//  chat
//
//  Created by Sergei Sevriugin on 11/09/16.
//  Copyright © 2016 MyAppTemplates. All rights reserved.
//

import Foundation
class MetaProvider: Provider {
    override init(sync: Synchronizer) {
        super.init(sync: sync, withName:"Meta", apiPath: "/application/meta")
    }
    override init(sync : Synchronizer, withDelegate delegate: ProviderDelegate) {
        super.init(sync: sync, withName:"Meta", apiPath: "/application/meta", withDelegate: delegate)
    }
    override func prepare(request: NSMutableURLRequest) {
        super.prepare(request)
        request.HTTPMethod = "GET"
    }
    func getUserInfo() -> UserInfo? {
        return self.sync.user
    }
}