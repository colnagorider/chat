//
//  ReGaUserProfile.swift
//  chat
//
//  Created by Sergei Sevriugin on 03/09/16.
//  Copyright © 2016 MyAppTemplates. All rights reserved.
//

import Foundation

enum ApplicationStatus {
    case Red
    case Yellow
    case Green
}

class ReGaUser : ProviderDelegate {
    
    var name: String?
    var avatar: String?
    
    var meta: [NSDictionary]?
    var application: [NSMutableDictionary]?
    
    var metaProvider: MetaProvider?
    var appProvider: ApplicationProvider?
    
    init() {
        // setup meta date, use isVisible as the filter and then sort it by the orderBy
        self.meta = self.resolve("ReGaUserProfileMeta").filter({$0["isVisible"] as! Int == 1}).sort({$1["orderBy"] as! Int > $0["orderBy"] as! Int})
        self.application = self.resolve("ReGaUserProfileApplication") as? [NSMutableDictionary]
        
        self.name = "User Profile"
        self.avatar = "profile"
        
        // add providers
        if let sync = Synchronizer.getSharedInstance() {
            // init meta provider
            self.metaProvider = MetaProvider(sync: sync, withDelegate: self)
            // init application provider
            self.appProvider = ApplicationProvider(sync: sync, withDelegate: self)
        }
    }
    
    subscript(index: Int) -> NSDictionary {
        get {
            return self.meta![index]
        }
    }
    
    func getStatus() -> ApplicationStatus {
        var result = ApplicationStatus.Green
        for item in self.meta! {
            if(item["kind"] as! Int == 9) {
                // skip sections
                continue
            }
            if let value = self.application!.filter({ $0["fieldId"] as! Int ==  item["id"] as! Int }) as [NSMutableDictionary]? {
                if(value.count == 0) {
                    if(item["groupId"] as! Int == 0) {
                        result = ApplicationStatus.Red
                    }
                    else {
                        result = ApplicationStatus.Yellow
                    }
                    break
                }
            }
            else {
                if(item["groupId"] as! Int == 0) {
                    result = ApplicationStatus.Red
                }
                else {
                    result = ApplicationStatus.Yellow
                }
                break
            }
        }
        print("Get Application status \(result)")
        return result
    }
    
    func isList(index: Int) -> Bool {
        if(self.meta![index]["items"] != nil)
        {
            return true
        }
        return false
    }
    
    func isList(field field: NSDictionary) -> Bool {
        if(field["items"] != nil)
        {
            return true
        }
        return false
    }
    
    func isGender(index: Int) -> Bool {
        if(self.meta![index]["kind"] as! Int == 6) {
            return true
        }
        return false
    }
    
    func isGender(field field: NSDictionary) -> Bool {
        if(field["kind"] as! Int == 6) {
            return true
        }
        return false
    }
    
    func isSection(index: Int) -> Bool {
        if(self.meta![index]["kind"] as! Int == 9) {
            return true
        }
        return false
    }
    
    func isSection(field field: NSDictionary) -> Bool {
        if(field["kind"] as! Int == 9) {
            return true
        }
        return false
    }
    
    func sectionCount() -> Int {
        return self.meta!.filter({ $0["kind"] as! Int == 9 }).count
    }
    
    func countSection(section: Int) -> Int {
        return self.meta!.filter({ $0["groupId"] as! Int ==  section}).count
    }
        
    func count() -> Int {
        return self.meta!.count
    }
    
    func itemAt(section: Int, row: Int) -> NSDictionary {
        return self.meta!.filter({ $0["groupId"] as! Int ==  section})[row]
    }
    
    
    func getFieldValue(fieldId: Int) -> NSMutableDictionary? {
        if let result = self.application!.filter({ $0["fieldId"] as! Int ==  fieldId}) as [NSMutableDictionary]? {
            if(result.count > 0) {
                return result[0]
            }
        }
        return nil
    }
    
    func setFieldValue(fieldId: Int, withString value: String) {
        if let current = self.getFieldValue(fieldId) {
            // update value
            current["value"] = value
        }
        else {
            // insert new value
            let newitem = ["fieldId":fieldId, "value":value] as NSMutableDictionary
            self.application!.append(newitem)
        }
    }
    
    func setFieldValue(fieldId: Int, withInt value: Int) {
        if let current = self.getFieldValue(fieldId) {
            // update value
            current["value"] = value
        }
        else {
            // insert new value
            let newitem = ["fieldId":fieldId, "value":value] as NSMutableDictionary
            self.application!.append(newitem)
        }
    }
    
    func resolve(source:String) -> [NSDictionary] {
        if let path = NSBundle.mainBundle().pathForResource(source, ofType: "json") {
            
            if let jsonData = NSData(contentsOfFile: path) {
                do {
                    if let jsonResult: NSDictionary = try NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary {
                        
                        if let responseResult : NSDictionary = jsonResult["result"] as? NSDictionary {
                            
                            if let fields: [NSDictionary] = responseResult["fields"] as? [NSDictionary] {
                                print("resolve \(source) fields : \(fields.count)")
                                return fields;
                            }
                        }
                    }
                }
                catch { print("Error while parsing: \(error)") }
            }
        }
        return []
    }
    
    func resolve() {
        if let provider = self.metaProvider {
            provider.resolve()
        }
        if let provider = self.appProvider {
            provider.resolve()
        }
    }
    
    func providerDataIsReady(name: String, data:[NSDictionary]?, status: ProviderStatus) {
        print("ReGaUserProfile providerDataIsReady \(name) data \(data) and status \(status)")
        
        var notification : String = "ToggleMetaNotification"
        var userInfo  = [NSObject:AnyObject]()
        
        if(status == ProviderStatus.Success) {
            // do data processing here ...

            if(name == "Meta") {
                // save loaded data
                if let meta = data {
                    self.meta = meta.filter({$0["isVisible"] as! Int == 1}).sort({$1["orderBy"] as! Int > $0["orderBy"] as! Int})
                }
            }
            else if(name == "Application") {
                // save loaded data
                if let app = data as! [NSMutableDictionary]? {
                    self.application = app.filter({ !($0["value"] is NSNull) })
                }
                notification = "ToggleApplicationNotification"
            }
            // post notification
            if let user = self.metaProvider?.getUserInfo() {
                if let name = user.name {
                    userInfo["name"] = name
                    self.name = name
                }
                if let avatar = user.avatar {
                    userInfo["avatar"] = avatar
                    self.avatar = avatar
                }
            }
            NSNotificationCenter.defaultCenter().postNotificationName(notification, object: nil, userInfo: userInfo)
        }
    }
}
