//
//  ApplicationProvider.swift
//  chat
//
//  Created by Sergei Sevriugin on 11/09/16.
//  Copyright © 2016 MyAppTemplates. All rights reserved.
//

import Foundation
class ApplicationProvider: Provider {
    override init(sync: Synchronizer) {
        super.init(sync: sync, withName:"Application", apiPath: "/application")
    }
    override init(sync : Synchronizer, withDelegate delegate: ProviderDelegate) {
        super.init(sync: sync, withName:"Application", apiPath: "/application", withDelegate: delegate)
    }
    override func prepare(request: NSMutableURLRequest) {
        super.prepare(request)
        request.HTTPMethod = "GET"
    }
    func getUserInfo() -> UserInfo? {
        return self.sync.user
    }
}