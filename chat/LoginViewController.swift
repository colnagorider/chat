//
//  LoginViewController.swift
//  Chat
//
//  Created by My App Templates Team on 24/08/14.
//  Copyright (c) 2014 My App Templates. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate , GIDSignInUIDelegate, ProviderDelegate, FBSDKLoginButtonDelegate {
    
    var parent: ChatViewController?
    var loginProvider : Provider?
    var googleProvider : GoogleProvider?
    var fbProvider: FBProvider?
    
    @IBOutlet var viewForContent : UIScrollView!
    @IBOutlet var viewForUser : UIView!
    @IBOutlet var txtForEmail : UITextField!
    @IBOutlet var txtForPassword : UITextField!
    @IBOutlet var fbLoginBtn: FBSDKLoginButton!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        // Custom initialization
        
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let size = UIScreen.mainScreen().bounds.size
        viewForContent.contentSize = CGSizeMake(size.width, 568)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let size = UIScreen.mainScreen().bounds.size
        viewForContent.contentSize = CGSizeMake(size.width, 568)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.willShowKeyBoard(_:)), name:UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.willHideKeyBoard(_:)), name:UIKeyboardWillHideNotification, object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.googleLogin(_:)), name:"ToggleAuthUINotification", object: nil)
        
        // Do any additional setup after loading the view.
        GIDSignIn.sharedInstance().uiDelegate = self
        
        fbLoginBtn.readPermissions = ["public_profile", "email", "user_friends"]
        
        // add providers
        if let sync = Synchronizer.getSharedInstance() {
            
            // init login provider
            self.loginProvider = Provider(sync: sync, withDelegate: self)
            
            // init google provider
            self.googleProvider = GoogleProvider(sync: sync, withDelegate: self)
            
            // init facebook provider
            self.fbProvider = FBProvider(sync: sync, withDelegate: self)
            
            // open session
            sync.open()
        }
    }
    
    func googleLogin(notification : NSNotification){
        if let userInfo = notification.userInfo {
            print("Google login \(userInfo)")
            if let provider = self.googleProvider {
                provider.sync([userInfo])
            }
        }
        else {
            print("Google login error")
        }
    }
    
    func willShowKeyBoard(notification : NSNotification){
    
        var userInfo: NSDictionary!
        userInfo = notification.userInfo

        var duration : NSTimeInterval = 0
        
        duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSTimeInterval
        let keyboardF:NSValue = userInfo.objectForKey(UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardFrame = keyboardF.CGRectValue()
        
        UIView.animateWithDuration(duration, delay: 0, options:[], animations: {
            self.viewForContent.contentOffset = CGPointMake(0, keyboardFrame.size.height)
            
            }, completion: nil)
       
    }
    
    func willHideKeyBoard(notification : NSNotification){
        
        var userInfo: NSDictionary!
        userInfo = notification.userInfo
        
        var duration : NSTimeInterval = 0
        duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSTimeInterval
        
        UIView.animateWithDuration(duration, delay: 0, options:[], animations: {
            self.viewForContent.contentOffset = CGPointMake(0, 0)
            
            }, completion: nil)
        
    }
    
    func textFieldShouldReturn (textField: UITextField) -> Bool{
        if ((textField == txtForEmail)){
            txtForPassword.becomeFirstResponder();
        } else if (textField == txtForPassword){
            textField.resignFirstResponder()
        }
        return true
    }
    
    @IBAction func registerBtnTapped() {
        self.dismissViewControllerAnimated(true, completion: nil);
        parent?.skipLogin = true
    }
    
    @IBAction func loginBtnTapped() {
        
        if let id = self.txtForEmail.text {
            if let credentials = self.txtForPassword.text {
                let userInfo:NSDictionary = ["id":id, "credentials":credentials]
                print ("loginBtnTapped userInfo \(userInfo)")
                if let provider = self.loginProvider {
                    provider.sync([userInfo])
                }
            }
        }
    }

    @IBAction func facebookBtnTapped() {
        self.dismissViewControllerAnimated(true, completion: nil);
        parent?.skipLogin = true
    }
    
    @IBAction func twitterBtnTapped() {
        self.dismissViewControllerAnimated(true, completion: nil);
        parent?.skipLogin = true
    }
    
    @IBAction func forgotPasswordBtnTapped() {
        self.dismissViewControllerAnimated(true, completion: nil);
        parent?.skipLogin = true
    }
    
    @IBAction func didTapSignOut(sender: AnyObject) {
        GIDSignIn.sharedInstance().signOut()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func providerDataIsReady(name: String, data:[NSDictionary]?, status: ProviderStatus) {
        print("LoginViewController providerDataIsReady \(name) data \(data) and status \(status)")
        
        if(status == ProviderStatus.Success) {
            self.dismissViewControllerAnimated(true, completion: nil);
            parent?.skipLogin = true
        }
    }
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        if(error == nil) {
            let userInfo:NSDictionary = ["clientId":result.token.userID, "accessToken":result.token.tokenString]
            print("Facebook logged in with userInfo :\(userInfo)")
            if let provider = self.fbProvider {
                provider.sync([userInfo])
            }
        }
        else {
            print("Facebook login error \(error.localizedDescription)")
        }
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        print("Facebook logged out")
    }

    /*
    // #pragma mark - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue?, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
