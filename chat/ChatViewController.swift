//
//  ChatViewController.swift
//  Chat
//
//  Created by My App Templates Team on 24/08/14.
//  Copyright (c) 2014 My App Templates. All rights reserved.
//

import UIKit

extension UIImage
{
    func roundImage() -> UIImage
    {
        let newImage = self.copy() as! UIImage
        let cornerRadius = self.size.height/2
        UIGraphicsBeginImageContextWithOptions(self.size, false, 1.0)
        let bounds = CGRect(origin: CGPointZero, size: self.size)
        UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).addClip()
        newImage.drawInRect(bounds)
        let finalImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return finalImage
    }
    
    func resizeImage(targetSize: CGSize) -> UIImage {
        let size = self.size
        
        let widthRatio  = targetSize.width  / self.size.width
        let heightRatio = targetSize.height / self.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSizeMake(size.width * heightRatio, size.height * heightRatio)
        } else {
            newSize = CGSizeMake(size.width * widthRatio,  size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRectMake(0, 0, newSize.width, newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.drawInRect(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
        
    }
}

extension UIImageView {
    public func imageFromUrl(urlString: String, withSize size: CGSize) {
        if let url = NSURL(string: urlString) {
            let request = NSURLRequest(URL: url)
            NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {
                (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
                self.image = UIImage(data: data!)?.roundImage().resizeImage(size)
                self.sizeToFit()
            }
        }
    }
}

class ChatViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var viewForTitle : UIView!
    @IBOutlet var ctrlForChat : UISegmentedControl!
    @IBOutlet var btnForLogo : UIButton!
    @IBOutlet var itemForSearch : UIBarButtonItem!
    @IBOutlet var tblForChat : UITableView!
    
    var user : ReGaUser?
    
    var skipIntro = false
    var skipLogin = false
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        // Custom initialization
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.titleView = viewForTitle
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: btnForLogo)
        self.navigationItem.rightBarButtonItem = itemForSearch
        self.tabBarController?.tabBar.tintColor = UIColor.greenColor()
        
        // Do any additional setup after loading the view.
        self.user = ReGaUser()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ChatViewController.metaLoaded(_:)), name:"ToggleMetaNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ChatViewController.appLoaded(_:)), name:"ToggleApplicationNotification", object: nil)
    }
    
    override func viewDidAppear(animated: Bool) {
        if(!skipLogin) {
           self.performSegueWithIdentifier("loginSegue", sender: nil)
        } else if(!skipIntro) {
            self.performSegueWithIdentifier("slideToIntro", sender: nil)
        } else {
            if let user = self.user {
                user.resolve()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func unwindToChat (segueSelected : UIStoryboardSegue) {
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0) {
            return 1
        }
        else {
            return self.user!.countSection(section - 1)
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView!) -> Int {
        return ( self.user!.sectionCount() + 1 )
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
        if (indexPath.row == 0 && indexPath.section == 0 ) {
            let cell = tblForChat.dequeueReusableCellWithIdentifier("ApplicationUser") as UITableViewCell!
            if let avatar = cell.subviews[0].subviews.filter({$0.tag == 1})[0] as? UIImageView {
                let targetSize = avatar.image?.size
                avatar.image = UIImage(named: self.user!.avatar!, inBundle: nil, compatibleWithTraitCollection: nil)
                if(avatar.image == nil) {
                    avatar.imageFromUrl(self.user!.avatar!, withSize: targetSize!)
                }
                avatar.sizeToFit()
            }
            if let name = cell.subviews[0].subviews.filter({$0.tag == 2})[0] as? UILabel {
                name.text = self.user!.name!
            }
            if let user = self.user {
                let status = user.getStatus()
                if let indicator = cell.subviews[0].subviews.filter({$0.tag == 0})[0] as? UIButton {
                    switch status {
                    case .Green:
                        indicator.tintColor = UIColor.greenColor()
                    case .Yellow:
                        indicator.tintColor = UIColor.yellowColor()
                    default:
                        indicator.tintColor = UIColor.redColor()
                    }
                }
            }
            return cell
        }
        
        let field = self.user!.itemAt(indexPath.section - 1, row: indexPath.row)
        
        if (self.user!.isSection(field: field)) {
            let cell = tblForChat.dequeueReusableCellWithIdentifier("ChatPrivateCell") as UITableViewCell!
            if let label = cell.subviews[0].subviews.filter({$0.tag == 1})[0] as? UILabel {
                label.text = field["name"] as! String?
            }
            return cell
        } else if (self.user!.isGender(field: field)) {
            let cell = tblForChat.dequeueReusableCellWithIdentifier("ApplicationRadio") as UITableViewCell!
            if let segment = cell.subviews[0].subviews.filter({$0.tag == 1})[0] as? UISegmentedControl {
                if let value = self.user!.getFieldValue(field["id"] as! Int) {
                    if let index = value["value"] as! Int? {
                        if(index < 2) {
                            segment.selectedSegmentIndex = index
                            
                            if let indicator = cell.subviews[0].subviews.filter({$0.tag == 0})[0] as? UIButton {
                                indicator.tintColor = UIColor.greenColor()
                            }
                        }
                    }
                }
                else {
                    segment.selectedSegmentIndex = 2
                    if let indicator = cell.subviews[0].subviews.filter({$0.tag == 0})[0] as? UIButton {
                        indicator.tintColor = UIColor.redColor()
                    }
                }
                segment.addTarget(self, action: #selector(segmentAction(_:)), forControlEvents: .ValueChanged)
            }
            return cell
        } else if (self.user!.isList(field: field)) {
            let cell = tblForChat.dequeueReusableCellWithIdentifier("ApplicationList") as UITableViewCell!
            if let label = cell.subviews[0].subviews.filter({$0.tag == 1})[0] as? UILabel {
                label.text = field["name"] as! String?
            }
            if let value = self.user!.getFieldValue(field["id"] as! Int) {
                if let items = field["items"] as! [NSDictionary]? {
                    if let item = items.filter({ $0["value"] as! Int == value["value"] as! Int }) as [NSDictionary]? {
                        if(item.count > 0) {
                            if let label = cell.subviews[0].subviews.filter({$0.tag == 2})[0] as? UILabel {
                                label.text = item[0]["name"] as! String?
                                if let indicator = cell.subviews[0].subviews.filter({$0.tag == 0})[0] as? UIButton {
                                    indicator.tintColor = UIColor.greenColor()
                                }
                            }
                        }
                    }
                }
            }
            else {
                if let indicator = cell.subviews[0].subviews.filter({$0.tag == 0})[0] as? UIButton {
                    if(indexPath.section == 1) {
                        indicator.tintColor = UIColor.redColor()
                    }
                    else {
                        indicator.tintColor = UIColor.yellowColor()
                    }
                }
            }
            return cell
        } else {
            let cell = tblForChat.dequeueReusableCellWithIdentifier("ApplicationInput") as UITableViewCell!
            if let label = cell.subviews[0].subviews.filter({$0.tag == 1})[0] as? UILabel {
                label.text = field["name"] as! String?
            }
            if let value = self.user!.getFieldValue(field["id"] as! Int) {
                if let input = cell.subviews[0].subviews.filter({$0.tag == 2})[0] as? UITextField {
                    input.text = value["value"] as! String?
                }
                if let indicator = cell.subviews[0].subviews.filter({$0.tag == 0})[0] as? UIButton {
                    indicator.tintColor = UIColor.greenColor()
                }
            }
            else {
                if let indicator = cell.subviews[0].subviews.filter({$0.tag == 0})[0] as? UIButton {
                    if(indexPath.section == 1) {
                        indicator.tintColor = UIColor.redColor()
                    }
                    else {
                        indicator.tintColor = UIColor.yellowColor()
                    }
                }
            }
            if let input = cell.subviews[0].subviews.filter({$0.tag == 2})[0] as? UITextField {
                input.delegate = self
            }
            return cell
        }
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if (identifier == "slideToSelect") {
            return false
        }
        else {
            return true
        }
    }
    
    func segmentAction(sender: UISegmentedControl) {
        print("segmentAction \(sender.selectedSegmentIndex)")
        if let user = self.user {
            if let superview = sender.superview {
                if let cell = superview.superview as! UITableViewCell? {
                    if(sender.selectedSegmentIndex < 2) {
                        if let index = self.tblForChat.indexPathForCell(cell) {
                            let field = user.itemAt(index.section - 1, row: index.row)
                            let value = sender.selectedSegmentIndex
                            user.setFieldValue(field["id"] as! Int, withInt: value)
                            self.updateStatus()
                            
                            if let indicator = cell.subviews[0].subviews.filter({$0.tag == 0}) as [UIView]? {
                                if(indicator.count > 0) {
                                    indicator[0].tintColor = UIColor.greenColor()
                                }
                            }
                            if let tblVew = self.tblForChat {
                                tblVew.reloadRowsAtIndexPaths([NSIndexPath(forRow: 0, inSection: 0), index], withRowAnimation: UITableViewRowAnimation.None)
                            }
                        }
                    }
                    else {
                        if let indicator = cell.subviews[0].subviews.filter({$0.tag == 0}) as [UIView]? {
                            if(indicator.count > 0) {
                                indicator[0].tintColor = UIColor.redColor()
                            }
                        }
                    }
                }
            }
        }
    }

    func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!){
        if(indexPath.row == 0 && indexPath.section == 0) {
            self.performSegueWithIdentifier("slideToChat", sender: nil);
        }
        else {
            let field = self.user!.itemAt(indexPath.section - 1, row: indexPath.row)
        
            if(self.user!.isList(field: field)) {
                self.performSegueWithIdentifier("slideToSelect", sender: field);
            }
            else if(!self.user!.isSection(field: field)) {
                self.performSegueWithIdentifier("slideToChat", sender: nil);
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let field = sender as! NSDictionary? {
            let dest    = segue.destinationViewController as! MoreTableViewController
            dest.items  = field["items"] as! [NSDictionary]?
            dest.field  = field
            dest.user   = self.user
        }
        if(segue.identifier == "slideToIntro") {
            print("slide to Intro")
            let dest    = segue.destinationViewController as! IntroViewController
            dest.parent = self
        } else if(segue.identifier == "loginSegue") {
            print("slide to Login")
            let dest    = segue.destinationViewController as! LoginViewController
            dest.parent = self
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        self.updateStatus()
        if let tblView = self.tblForChat {
            if let index = tblView.indexPathsForSelectedRows {
                var indexToUpdate = index
                indexToUpdate.append(NSIndexPath(forRow: 0, inSection: 0))
                tblView.reloadRowsAtIndexPaths(indexToUpdate, withRowAnimation: UITableViewRowAnimation.None)
            }
            else {
                tblView.reloadData()
            }
        }
    }
    
    func updateStatus() {
        if let user = self.user {
            if let tbl = self.tblForChat {
                if let cell = tbl.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)) {
                    let status = user.getStatus()
                    if let indicator = cell.subviews[0].subviews.filter({$0.tag == 0})[0] as? UIButton {
                        switch status {
                        case .Green:
                            indicator.tintColor = UIColor.greenColor()
                        case .Yellow:
                            indicator.tintColor = UIColor.yellowColor()
                        default:
                            indicator.tintColor = UIColor.redColor()
                        }
                    }
                }
            }
        }
    }
    
    
    func textFieldDidEndEditing(textField: UITextField) {
        if let superview = textField.superview {
            if let cell = superview.superview as! UITableViewCell? {
                if let index = self.tblForChat.indexPathForCell(cell) {
                    if let user = self.user {
                        let field = user.itemAt(index.section - 1, row: index.row)
                        if let value = textField.text {
                            user.setFieldValue(field["id"] as! Int, withString: value)
                            self.updateStatus()
                            print("Text value \(value) for \(field["id"] as! Int) \(field["name"] as! String)")
                            
                        }
                    }
                    if let indicator = cell.subviews[0].subviews.filter({$0.tag == 0}) as [UIView]? {
                        if(indicator.count > 0) {
                            indicator[0].tintColor = UIColor.greenColor()
                        }
                    }
                    if let tblView = self.tblForChat {
                        tblView.reloadRowsAtIndexPaths([NSIndexPath(forRow: 0, inSection: 0), index], withRowAnimation: UITableViewRowAnimation.None)
                    }
                }
            }
        }
    }

    func metaLoaded(notification : NSNotification){
        print("ChatViewController metaLoaded \(notification.userInfo)")
    }
    
    func appLoaded(notification : NSNotification){
        print("ChatViewController appLoaded \(notification.userInfo)")
        // reload data
        if let tblView = self.tblForChat {
            tblView.reloadData()
        }
    }
    
    func ResizeAndRoundImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSizeMake(size.width * heightRatio, size.height * heightRatio)
        } else {
            newSize = CGSizeMake(size.width * widthRatio,  size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRectMake(0, 0, newSize.width, newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.drawInRect(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    
    /*
    // #pragma mark - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue?, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
