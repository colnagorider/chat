//
//  GoogleProvider.swift
//  chat
//
//  Created by Sergei Sevriugin on 08/09/16.
//  Copyright © 2016 MyAppTemplates. All rights reserved.
//

import Foundation
class GoogleProvider: Provider {
    let redirectUri:String = "http://rega.site"
    let clientId:String = "903339301761-ubt1veuver343s1c38bphbpjc5frhggu.apps.googleusercontent.com"
    override init(sync: Synchronizer) {
        super.init(sync: sync, withName:"Google", apiPath: "/auth/google")
    }
    override init(sync : Synchronizer, withDelegate delegate: ProviderDelegate) {
        super.init(sync: sync, withName:"Google", apiPath: "/auth/google", withDelegate: delegate)
    }
    override func sync(data: [NSDictionary]) {
        let google_data             = NSMutableDictionary()
        google_data["id"]           = data[0]["userId"]
        google_data["code"]         = data[0]["serverAuthCode"]
        google_data["clientId"]     = self.clientId
        google_data["redirectUri"]  = self.redirectUri
        super.sync([google_data])
    }
}
