//
//  IntroViewController.swift
//  chat
//
//  Created by Sergei Sevriugin on 06/09/16.
//  Copyright © 2016 MyAppTemplates. All rights reserved.
//

import UIKit

class IntroViewController : UIViewController, UIWebViewDelegate {
    
    var parent: ChatViewController?
    var showHelp = false
    var loadLocal = false
    
    @IBOutlet var webView : UIWebView!
    @IBOutlet var indicatorView : UIActivityIndicatorView!
    @IBOutlet var cancelBtn: UIButton!
    @IBOutlet var helpBtn: UIButton!
    @IBOutlet var helpView: UIView!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        // Custom initialization
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // setup cancel button
        cancelBtn.addTarget(self, action: #selector(cancelAction(_:)), forControlEvents: .TouchDown)
        // setup help button
        helpBtn.addTarget(self, action: #selector(helpAction(_:)), forControlEvents: .TouchDown)
        
        self.view.autoresizingMask = [.FlexibleRightMargin, .FlexibleLeftMargin, .FlexibleBottomMargin, .FlexibleWidth,.FlexibleHeight, .FlexibleTopMargin]
        
        let localfilePath = NSBundle.mainBundle().URLForResource("intro", withExtension: "html");
        let localRequest = NSURLRequest(URL: localfilePath!);
        
        let url = NSURL(string: "http://rega.site")
        let requestObj = NSURLRequest(URL: url!, cachePolicy: .UseProtocolCachePolicy, timeoutInterval: 10)
        
        // webView.loadRequest(requestObj)
        if(self.loadLocal) {
            webView.loadRequest(localRequest)
        }
        else {
            webView.loadRequest(requestObj)
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        print("Web View shouldStartLoadWithRequest \(request.allHTTPHeaderFields)")
        return true
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        print("Web View Did Start Load")
        if let indicator = self.indicatorView {
            indicator.startAnimating()
        }
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        print("Web View Did Finish Load")
        if let indicator = self.indicatorView {
            indicator.stopAnimating()
        }
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        if let msg = error!.localizedFailureReason {
            print("Load Web View Failes with error \(msg)")
        }
        print("Loading local copy...")
        let localfilePath = NSBundle.mainBundle().URLForResource("intro", withExtension: "html");
        let localRequest = NSURLRequest(URL: localfilePath!);
        webView.loadRequest(localRequest)
    }
    
    func cancelAction(sender: UIButton) {
        if let indicator = self.indicatorView {
            indicator.stopAnimating()
        }
        if let parent = self.parent {
            print("Cancel Button is pressed")
            parent.skipIntro = true
            self.dismissViewControllerAnimated(true, completion: nil);
        }
    }
    
    func helpAction(sender: UIButton) {
        print("Help Button is pressed")
        if(self.showHelp) {
            self.helpView.hidden = true
            self.showHelp = false
        }
        else {
            self.helpView!.hidden = false
            self.showHelp = true
        }
        
    }
    
}